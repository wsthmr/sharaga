#include "Table.h"
using namespace std;
void Table::Add(string &street, string &parking, string &number, int gps, Type_operation violation, Type_operation type_auto)
{
	this->street = street;
	this->parking = parking;
	this->gps = gps;
	this->number = number;
	this->violation = violation;
	this->type_auto = type_auto;
}

Table &Table::getObject()
{
	static Table object;
	return object;
}

//street
string Table::getStreet()
{
	return this->street;
}
//parking
string Table::getParking()
{
	return this->parking;
}
//gps
int Table::getGPS()
{
	return this->gps;
}
//number
string Table::getNumber()
{
	return this->number;
}
//violation
Table::Type_operation Table::getViolation()
{
	return this->violation;
}
string Table::getViolationToStr()
{
	return Table::getViolation() == Table::income ? "camp on the roadway in the place of prohibition" : "standing on the sidewalk";
}
//type_auto
Table::Type_operation Table::getType_Auto()
{
	return this->type_auto;
}
string Table::getType_AutoToStr()
{
	return Table::getType_Auto() == Table::income ? "passenger" : "trucks light-duty";
}
