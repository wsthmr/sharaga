#include "Menu.h"
#include <iostream>
using namespace std;
void init() {
	char key;
	do {
		clear();
		printf("________________________ENTER AN ACTION..______________________\n\n");
		printf("1) Add information\n");
		printf("2) Print information\n");
		printf("3) Edit information\n");
		printf("4) Exit\n");

		cin >> key;
		switch (key)
		{
		case '1':	clear(); Menu::getObject().inputData(); cin.get(); break;
		case '2':	clear(); Menu::getObject().outputData(); cin.get(); break;
		case '3':	clear(); Menu::getObject().editData();  cin.get();break;
		case '4':  break;
		}
	} while (key != '4');
}

int main() {
	init(); 
	return 0;
}
