#pragma once
#include <iostream>
#include <stdio.h>
#include <iomanip>
#ifndef __linux
#include <conio.h>
#include <cstdlib>
void  inline clear() {
	system("cls");
}
#else
#include <unistd.h>
#include <termios.h>
#include <cstdlib>

void inline clear()
{
	system("clear");
}
#endif