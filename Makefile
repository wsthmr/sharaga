﻿
main.o: main.cpp
	g++ -c  main.cpp
Table.o: Table.cpp
	g++ -c  Table.cpp
Menu.o: Menu.cpp
	g++ -c -fPIC  Menu.cpp
start: main.cpp Table.cpp Menu.cpp
	g++ -o start main.cpp Table.cpp Menu.cpp
	./start
unittest: unittest.cpp Table.cpp
	g++ -o unittest unittest.cpp Table.cpp
	./unittest
cppcheck:
	cppcheck --enable=all --language=c++ main.cpp Table.cpp Menu.cpp Table.h Menu.h Libs.h
clean:
	rm  *.o *.a unittest start static_build  dynamic_build
valgrind:
	valgrind  ./start

#Компиляция статической библиотеки
static_build: main.o Menu.o libstatic.a
	g++ -o static_build  main.o Menu.o  -L. -lstatic
libstatic.a: Table.o Menu.o
	ar cr libstatic.a Table.o

#Компиляция динамической библиотеки
dynamic_build: main.o Table.o libdynamic.so
	g++ -o dynamic_build main.o Table.o -L. -ldynamic -Wl,-rpath,.
libdynamic.so: Menu.o
	g++ -shared -o libdynamic.so Menu.o

