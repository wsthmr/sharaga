#pragma once
#include <vector>
#include <string>

class Table
{
public:
	enum Type_operation
	{
		income,
		expense
	};
	Table() : street(""), parking(""), number(""), gps(), violation((Type_operation)0), type_auto((Type_operation)0){};
	static Table &getObject();
	void Add(std::string &street, std::string &parking, std::string &number, int gps, Type_operation violation, Type_operation type_auto);
	bool Delete(const size_t &number, std::vector<Table> &vec);
	std::string getStreet();
	std::string getParking();
	std::string getNumber();
	int getGPS();
	Type_operation getViolation();
	Type_operation getType_Auto();
	std::string getViolationToStr();
	std::string getType_AutoToStr();
	~Table() {}

private:
	std::string street, parking, number;
	int gps;
	Type_operation violation, type_auto;
};