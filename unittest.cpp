#include <iostream>
#include "Table.h"
#include <string>
using namespace std;
bool checkInitParameters()
{
	if (
			Table::getObject().getStreet() == "" &&
			Table::getObject().getParking() == "" &&
			Table::getObject().getNumber() == "" &&
			Table::getObject().getGPS() == 0 &&
			Table::getObject().getViolation() == (Table::Type_operation)0 &&
			Table::getObject().getType_Auto() == (Table::Type_operation)0)
		return true;
	else
		return false;
}
bool checkAdd(string &street, string &parking, string &number, int gps, Table::Type_operation Violation, Table::Type_operation type_auto)
{
	bool flag = false;
	Table::getObject().Add(street, parking, number, gps, Violation, type_auto);
	flag = true;
	return flag;
}
bool checkGetStreet(string &street)
{
	if (Table::getObject().getStreet() == street)
		return true;
	else
		return false;
}

bool checkGetParking(string &parking)
{
	if (Table::getObject().getParking() == parking)
		return true;
	return false;
}

bool checkGetNumber(string &number)
{
	if (Table::getObject().getNumber() == number)
		return true;
	return false;
}

bool checkGetGPS(int gps)
{
	if (Table::getObject().getGPS() == gps)
		return true;
	return false;
}

bool checkGetViolation(Table::Type_operation violation)
{
	if (Table::getObject().getViolation() == violation)
		return true;
	else
		return false;
}

bool checkGetViolationToStr()
{
	if (Table::getObject().getViolation() == Table::income)
	{
		if (Table::getObject().getViolationToStr() == "camp on the roadway in the place of prohibition")
			return true;
	}
	else
	{
		if (Table::getObject().getViolationToStr() == "standing on the sidewalk")
			return true;
	}
	return false;
}

bool checkGettype_auto(Table::Type_operation type_auto)
{
	if (Table::getObject().getType_Auto() == type_auto)
		return true;
	else
		return false;
}

bool checkGettype_autoToStr()
{
	if (Table::getObject().getType_Auto() == Table::income)
	{
		if (Table::getObject().getType_AutoToStr() == "passenger")
			return true;
	}
	else
	{
		if (Table::getObject().getType_AutoToStr() == "trucks light-duty")
			return true;
	}
	return false;
}

int main()
{
	string street = "street",
				 parking = "parking",
				 number = "123adv";
	int gps = 1,
			vector<Table> vec = {Table::getObject()};
	Table::Type_operation violation = Table::income;
	Table::Type_operation type_auto = Table::income;

	if (checkInitParameters())
		cout << "Test \"checkInitParameters\" passed.\n";
	else
		cout << "Test \"checkInitParameters\" error.\n";

	if (checkAdd(street, parking, number, gps, violation, type_auto))
		cout << "Test \"CheckAdd\" passed.\n";
	else
		cout << "Test \"CheckAdd\" error.\n";

	if (checkGetStreet(street))
		cout << "Test \"checkGetStreet\" passed.\n";
	else
		cout << "Test \"checkGetStreet\" error.\n";

	if (checkGetParking(parking))
		cout << "Test \"checkGetParking\" passed.\n";
	else
		cout << "Test \"checkGetParking\" error.\n";

	if (checkGetNumber(number))
		cout << "Test \"checkGetNumber\" passed.\n";
	else
		cout << "Test \"checkGetNumber\" error.\n";

	if (checkGetGPS(gps))
		cout << "Test \"checkGetGPS\" passed.\n";
	else
		cout << "Test \"checkGetGPS\" error.\n";

	if (checkGetViolation(violation))
		cout << "Test \"checkGetViolation\" passed.\n";
	else
		cout << "Test \"checkGetViolation\" error.\n";

	if (checkGetViolationToStr())
		cout << "Test \"checkGetViolationToStr\" passed.\n";
	else
		cout << "Test \"checkGetViolationToStr\" error.\n";

	if (checkGettype_auto(type_auto))
		cout << "Test \"checkGettype_auto\" passed.\n";
	else
		cout << "Test \"checkGettype_auto\" error.\n";

	if (checkGettype_autoToStr())
		cout << "Test \"checkGettype_autoToStr\" passed.\n";
	else
		cout << "Test \"checkGettype_autoToStr\" error.\n";

	return 0;
}